import React from 'react';
import {Provider} from "react-redux";
import thunkMiddleware from 'redux-thunk';
import {applyMiddleware, combineReducers, createStore} from "redux";

import OrderPizza from "./src/Containers/OrderPizza";
import dishesReducer from "./src/store/reducers/dishesReducer";
import ordersReducer from "./src/store/reducers/ordersReducer";

const rootReducer = combineReducers({
  dishes: dishesReducer,
  orders: ordersReducer,
});
const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <OrderPizza/>
      </Provider>
    );
  }
}
