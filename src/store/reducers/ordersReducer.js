import * as actionTypes from '../actions/actionTypes';

const initialState = {
  orders: {},
  totalPrice: 0,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.IS_ORDER:
      const orders = {...state.orders};
      orders[action.key] ? orders[action.key] += 1 : orders[action.key] = 1;
      return {...state,
        totalPrice: state.totalPrice + parseInt(action.dish.price),
        orders
      };
    case actionTypes.CLEAR_ORDERS:
      return {orders: {}, totalPrice: 0};
    case actionTypes.REMOVE_ORDER:
      const newOrders = {...state.orders};
      delete newOrders[action.key];
      return {...state, orders: newOrders, totalPrice: state.totalPrice - (action.price * state.orders[action.key])};
    default:
      return state;
  }
};

export default reducer;