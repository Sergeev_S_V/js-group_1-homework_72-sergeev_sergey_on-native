import * as actionTypes from "./actionTypes";
import axios from '../../axios-order-pizza';

export const isOrder = (key, dish) => {
  return {type: actionTypes.IS_ORDER, key, dish};
};

export const removeOrder = (key, price) => {
  return {type: actionTypes.REMOVE_ORDER, key, price};
};

export const clearOrders = () => {
  return {type: actionTypes.CLEAR_ORDERS};
};

export const sendOrder = order => dispatch => {
  dispatch(clearOrders());
  axios.post(`orders.json`, order);
};