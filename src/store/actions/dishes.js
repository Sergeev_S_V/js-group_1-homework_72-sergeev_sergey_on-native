import * as actionTypes from './actionTypes';
import axios from '../../axios-order-pizza';

export const getData = () => dispatch => {
  dispatch(request());
  axios.get(`dishes.json`)
    .then(resp => resp ? dispatch(successResponse(resp.data)) : null)
};

export const request = () => {
  return {type: actionTypes.REQUEST};
};

export const successResponse = dishes => {
  return {type: actionTypes.SUCCESS_RESPONSE, dishes};
};

