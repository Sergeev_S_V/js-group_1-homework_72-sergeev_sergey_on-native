export const REQUEST = 'REQUEST';
export const SUCCESS_RESPONSE = 'SUCCESS_RESPONSE';
export const IS_ORDER = 'IS_ORDER';
export const REMOVE_ORDER = 'REMOVE_ORDER';
export const CLEAR_ORDERS = 'CLEAR_ORDERS';