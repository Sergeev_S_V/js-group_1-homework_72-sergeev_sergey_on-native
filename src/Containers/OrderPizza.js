import React, {Component} from 'react';
import {Button, FlatList, Modal, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {getData} from "../store/actions/dishes";
import Dish from "../Components/Dish/Dish";
import {isOrder, removeOrder, sendOrder} from "../store/actions/orders";

const costDelivery = 150;

class OrderPizza extends Component {

  state = {
    modalVisible: false
  };

  componentDidMount() {
    this.props.getData();
  };

  toggleModal = () => {
    this.setState(prevState => {
      return {modalVisible: !prevState.modalVisible}
    });
  };

  isDisabled = () => {
    const amount = Object.keys(this.props.orders).reduce((acc, key) => {
      acc += this.props.orders[key];
      return acc;
    }, 0);
    return amount <= 0;
  };

  formOrder = () => {
    this.toggleModal();
    return Object.keys(this.props.orders).reduce((order, key) => {
          order[key] = {
            amount: this.props.orders[key],
            cost: this.props.dishes[key].price,
          };
          return order;
        }, {});
  };

  render() {
    let title;
    if (Object.keys(this.props.orders).length === 0) {
      title = <Text>Orders is empty</Text>
    } else {
      title = <Text>Your orders: </Text>
    }
    return(
      <View style={styles.container}>
        <Text style={styles.title}>Turtle Pizza</Text>
        <Modal visible={this.state.modalVisible}
               onRequestClose={() => this.toggleModal()}>
          <View>
            {title}
            {Object.keys(this.props.orders).map(key => (
                <View key={key} style={styles.orders}>
                  <Text style={styles.order}>{this.props.dishes[key].title} x {this.props.orders[key]} {this.props.dishes[key].price * this.props.orders[key]} KGS</Text>
                  <TouchableOpacity style={styles.trash} onPress={() => this.props.removeOrder(key, this.props.dishes[key].price)}>
                    <Text>Delete</Text>
                  </TouchableOpacity>
                </View>
              ))}
            <Text>Delivery {costDelivery} KGS</Text>
            <Text>Total: {this.props.totalPrice + costDelivery}</Text>
            <Button title='Cancel' onPress={this.toggleModal}/>
            <Button disabled={this.isDisabled()} title='Order' onPress={() => this.props.sendOrder(this.formOrder())}/>
          </View>

        </Modal>
        <FlatList
          data={Object.keys(this.props.dishes)}
          keyExtractor={(item, index) => index}
          renderItem={({item}) => {
            return <Dish clicked={() => this.props.isOrder(item, this.props.dishes[item])}
                         // Можно сразу передать this.props.dishes[item] в isOrder
                  title={this.props.dishes[item].title}
                  price={this.props.dishes[item].price}
                  image={this.props.dishes[item].image}
            />
          }}
        />
        <View>
          <Text>Order total: {this.props.totalPrice} KGS</Text>
          <Button title='Checkout' onPress={this.toggleModal}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginTop: 23,
    marginBottom: 10,
  },
  trash: {
    borderWidth: 1,
    width: 50,
    alignItems: 'center',
    backgroundColor: 'lightblue',
    borderRadius: 5,
  },
  order: {
    width: '100%',
    marginBottom: 10,
  },
  orders: {
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    padding: 10,
  }
});

const mapStateToProps = state => {
  return {
    dishes: state.dishes.dishes,
    orders: state.orders.orders,
    totalPrice: state.orders.totalPrice,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getData: () => dispatch(getData()),
    isOrder: (key, dish) => dispatch(isOrder(key, dish)),
    removeOrder: (key, price) => dispatch(removeOrder(key, price)),
    sendOrder: (order) => dispatch(sendOrder(order)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderPizza);