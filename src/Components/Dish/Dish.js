import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from "react-native";

const Dish = props => (
  <TouchableOpacity onPress={props.clicked}>
    <View style={styles.Dish}>
      <Image resizeMode="contain" source={{uri: props.image}} style={styles.image} />
      <Text>{props.title}</Text>
      <Text> {props.price} KGS</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  Dish: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10
  },
  image: {
    width: 150,
    height: 150,
    marginRight: 10
  }
});

export default Dish;